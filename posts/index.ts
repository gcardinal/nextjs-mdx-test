import { join } from 'path';
import { readdirSync } from 'fs';

const postsDirectory = join(process.cwd(), 'posts');

export const getPostModulePath = (filename: string) => join('@/posts', `${filename}.mdx`);

export const getAllPostsSlugs = (): string[] => {
  const postFiles = readdirSync(postsDirectory).filter((filename) => !filename.includes('index'));

  return postFiles.map((filename) => filename.split('.')[0]);
};
