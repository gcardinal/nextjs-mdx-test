import React, { ReactNode } from 'react';
import { GetStaticPaths, GetStaticProps, NextPage } from 'next';
import { getAllPostsSlugs } from '@/posts';
import dynamic from 'next/dynamic';

export interface PostProps {
  slug: ReactNode
}

export interface PostParams {
  slug: string
}

const Post: NextPage<PostProps> = ({ slug }) => {
  const DynamicPost = dynamic(() => import(`../../posts/${slug}.mdx`));

  return (
      <div>
        <p>In some layout</p>
        <DynamicPost/>
      </div>
  );
};

export const getStaticProps: GetStaticProps<PostProps, PostParams> = async ({ params }) => Promise.resolve({
  props: {
    slug: params.slug,
  },
});

export const getStaticPaths: GetStaticPaths<PostProps> = async () => Promise.resolve({
  paths: getAllPostsSlugs().map((slug) => ({
    params: {
      slug,
    },
  })),
  fallback: false,
});

export default Post;
