NextJS + MDX Test
===

Stupid simple project to validate that MDX can be used as an easy way to handle content for static websites where we
want more than the limited formatting Markdown allows.

## Run + Install
`yarn && yarn dev`. Your terminal will output where the app is running.

To see the rendered MDX, go to `localhost:3000/blog/first` or `localhost:3000/blog/second`. You can also add "posts" by
adding MDX files in the `posts` directory. They will be available via their filename.

## Next Steps?
There won't be any, but if there were, there are a few things we might want:
- Metadata directly in the MDX file, [probably using grey-matter][1].

[1]: https://www.npmjs.com/package/gray-matter
